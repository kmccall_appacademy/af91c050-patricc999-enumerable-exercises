require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  if arr == []
    return 0
  end
  arr.reduce { |num, sum| sum + num }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_string? helper method
def in_all_strings?(long_strings, substring)
  long_strings == long_strings
    .select { |string| sub_string?(string, substring) }
end

def sub_string?(string, substring)
  string.include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  uniques = string.chars
    .select { |letter| string.chars.count(letter) == 1 }
  non_uniques = string.chars
    .reject { |letter| uniques.include?(letter) }
  non_uniques.uniq.sort[1..-1]
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split(' ')
  sorted_words = words.sort_by { |word| word.length }
  sorted_words[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ('a'..'z').to_a
  alphabet.reject { |letter| string.include?(letter) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  range = (first_yr..last_yr).to_a
  range.select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year_array = year.to_s.chars
  year_array == year_array.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  repeats = []
  songs.each_with_index do |song, idx|
    if song == songs[idx + 1]
      repeats.push(song)
    end
  end
  wonders = songs.reject { |song| repeats.include?(song) }
  wonders.uniq
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  longest_c = 'couldntfigureouthowtomakelowindex'
  clean_string = remove_punctuation(string)
  clean_string.split(' ').each do |word|
    if word.chars.include?('c')
      if c_distance(word) < c_distance(longest_c)
        longest_c = word
      end
    end
  end
  longest_c
end

def remove_punctuation(string)
  punctuation = ['.', ',', '!', '?']
  clean_string = string.chars
    .reject { |char| punctuation.include?(char) }
  clean_string.join()
end

def c_distance(word)
  c_index = 0
  word.chars.each_with_index do |letter, idx|
    if letter == 'c'
      c_index = idx
    end
  end
  word.length - c_index
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  starting_index = nil
  arr.each_with_index do |num, idx|
    if num == arr[idx + 1]
      starting_index = idx unless starting_index != nil
    elsif starting_index != nil
      ending_index = idx
      ranges.push([starting_index, ending_index])
      starting_index = nil
    end
  end
  ranges
end
